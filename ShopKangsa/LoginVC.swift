//
//  LoginVC.swift
//  ShopKangsa
//
//  Created by Nghia Pham on 19/02/2022.
//

import UIKit

class LoginVC: UIViewController {

    @IBOutlet weak var btnGoogle: UIButton!
    @IBOutlet weak var nameShopLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        //draw shapeOval
        let rect:CGRect = CGRect(x: -158, y: -200, width: 596 , height: 489)
        let shapeLayer = OvalExtension.drawOval(rect: rect, fillColor: UIColor.hexStringToUIColor(hex: "#4CAD73"), strokeColor: UIColor.white)
        self.view.layer.insertSublayer(shapeLayer, at: 0)
        
        configViewController()
    }
    func configViewController(){
        nameShopLabel.font = UIFont.systemFont(ofSize: 28, weight: .semibold)
        //btnGoogle.padding
    }
    
}
