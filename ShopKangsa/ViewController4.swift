//
//  ViewController4.swift
//  ShopKangsa
//
//  Created by Nghia Pham on 04/03/2022.
//

import UIKit
import WebKit
class ViewController4: UIViewController , WKNavigationDelegate, WKUIDelegate, WKScriptMessageHandler {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        
    }
    

    @IBOutlet weak var webview: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
//        let config = WKWebViewConfiguration()
//        let js = getMyJavaScript()
//        let script = WKUserScript(source: js, injectionTime: .atDocumentEnd, forMainFrameOnly: false)
//
//        config.userContentController.addUserScript(script)
//        config.userContentController.add(self, name: "clickListener")
//
//        webview = WKWebView(frame: view.bounds, configuration: config)
//        webview.uiDelegate = self
//        webview.navigationDelegate = self

    }
    override func viewDidAppear(_ animated: Bool) {
        let myProjectBundle:Bundle = Bundle.main
               
       let myUrl = myProjectBundle.url(forResource: "index", withExtension: "html")!
        webview.loadFileURL(myUrl,allowingReadAccessTo: myUrl)
    }
    func getMyJavaScript() -> String {
          if let filepath = Bundle.main.path(forResource: "demo-script", ofType: "js") {
              do {
                  return try String(contentsOfFile: filepath)
              } catch {
                  return ""
              }
          } else {
             return ""
          }
      }
    
}
