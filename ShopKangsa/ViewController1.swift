//
//  ViewController1.swift
//  ShopKangsa
//
//  Created by Nghia Pham on 04/03/2022.
//

import UIKit

class ViewController1: UIViewController {
    var delegate:sendProtocol? = nil
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func btnSend(_ sender: Any) {
        delegate?.returnlog()
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
      let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ViewController2") as! ViewController2
      nextViewController.modalPresentationStyle = .fullScreen
      nextViewController.delegate = delegate
      self.present(nextViewController, animated:true, completion:nil)

    }
}
