//
//  OvalExtension.swift
//  ShopKangsa
//
//  Created by Nghia Pham on 19/02/2022.
//

import UIKit

class OvalExtension: UIView {
    
    static func drawOval(rect :CGRect, fillColor:UIColor ,strokeColor:UIColor)-> CAShapeLayer{
        
        let ellipsePath = UIBezierPath(ovalIn: rect)
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = ellipsePath.cgPath
        shapeLayer.fillColor = fillColor.cgColor
        shapeLayer.strokeColor = strokeColor.cgColor
        shapeLayer.lineWidth = 5.0
        
        return shapeLayer
    }
}

